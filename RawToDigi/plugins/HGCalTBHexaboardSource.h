#include "FWCore/Framework/interface/Event.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Sources/interface/ProducerSourceFromFiles.h"
#include "FWCore/ParameterSet/interface/ConfigurationDescriptions.h"
#include "FWCore/ParameterSet/interface/ParameterSetDescription.h"
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <string>
#include <sstream>
#include "stdlib.h"
#include "HGCal/CondObjects/interface/HGCalElectronicsMap.h"
/**
 * \class HGCalTBHexaboardSource HGCal/RawToDigi/plugins/HGCalTBHexaboardSource.h
 *
 * \brief convert data from raw file to HGCalTBSkiroc2CMS data
 *
 */

class HGCalTBHexaboardSource : public edm::ProducerSourceFromFiles
{

 public:
  explicit HGCalTBHexaboardSource(const edm::ParameterSet & pset, edm::InputSourceDescription const& desc);

  virtual ~HGCalTBHexaboardSource(){;}

  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

 private:
  bool setRunAndEventInfo(edm::EventID& id, edm::TimeValue_t& time, edm::EventAuxiliary::ExperimentType&);
  virtual void produce(edm::Event & e);
  virtual void endJob() override;

  //bool readRaw(void);
  std::vector< std::array<uint16_t,1924> > decode_raw(const char* raw);
 private:
  std::string m_filePath;
  std::string m_fileName;
  std::string m_electronicMap;
  std::string m_outputCollectionName;
  unsigned int m_nWords;
  unsigned int m_headerSize;
  unsigned int m_eventTrailerSize;
  unsigned int m_nSkipEvents;
  bool m_compressedData;

  unsigned int m_event;
  unsigned int m_fileId;
  
  char* m_buffer;
  char* m_header;
  uint16_t m_dacInj;
  std::ifstream m_input;
  std::vector< std::array<uint16_t,1924> > m_decodedData;
  struct {
    HGCalElectronicsMap emap_;
  } essource_;
};
